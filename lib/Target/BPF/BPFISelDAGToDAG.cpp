//===-- BPFISelDAGToDAG.cpp - A dag to dag inst selector for BPF ----------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file defines a DAG pattern matching instruction selector for BPF,
// converting from a legalized dag to a BPF dag.
//
//===----------------------------------------------------------------------===//

#include "BPF.h"
#include "BPFRegisterInfo.h"
#include "BPFSubtarget.h"
#include "BPFTargetMachine.h"
#include "llvm/CodeGen/MachineConstantPool.h"
#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/CodeGen/MachineRegisterInfo.h"
#include "llvm/CodeGen/SelectionDAGISel.h"
#include "llvm/IR/IntrinsicInst.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Support/CommandLine.h"
using namespace llvm;

static cl::opt<unsigned long long> BPFGuestOffset("bpf-guest-offset",
		                        cl::desc("BPF guest pointers offset"),
                            cl::init(0), cl::Hidden);
static cl::opt<int> BPFGuestWidth("bpf-guest-width",
                                  cl::desc("BPF guest pointers address width"),
                                  cl::init(0), cl::Hidden);
static cl::opt<bool> BPFGuest("bpf-guest",
		                          cl::desc("BPF guest support"),
                              cl::init(false), cl::Hidden);


#define DEBUG_TYPE "bpf-isel"

// Instruction Selector Implementation
namespace {

class BPFDAGToDAGISel : public SelectionDAGISel {
public:
  explicit BPFDAGToDAGISel(BPFTargetMachine &TM) : SelectionDAGISel(TM) {}

  StringRef getPassName() const override {
    return "BPF DAG->DAG Pattern Instruction Selection";
  }

private:
// Include the pieces autogenerated from the target description.
#include "BPFGenDAGISel.inc"

  void Select(SDNode *N) override;

  // Complex Pattern for address selection.
  bool SelectAddr(SDNode *Parent, SDValue Addr, SDValue &Base, SDValue &Offset);
  bool SelectFIAddr(SDValue Addr, SDValue &Base, SDValue &Offset);
  bool AdjustGuestPointer(MemSDNode *node, SDValue &Ptr, SDValue &Offset, SDValue &Chain);
};
}

// ComplexPattern used on BPF Load/Store instructions
bool BPFDAGToDAGISel::SelectAddr(SDNode *Parent, SDValue Addr, SDValue &Base,
                                 SDValue &Offset) {
  // if Address is FI, get the TargetFrameIndex.
  SDLoc DL(Addr);
  if (FrameIndexSDNode *FIN = dyn_cast<FrameIndexSDNode>(Addr)) {
    Base   = CurDAG->getTargetFrameIndex(FIN->getIndex(), MVT::i64);
    Offset = CurDAG->getTargetConstant(0, DL, MVT::i64);
    return true;
  }

  if (Addr.getOpcode() == ISD::TargetExternalSymbol ||
      Addr.getOpcode() == ISD::TargetGlobalAddress)
    return false;

  MemSDNode *Node = cast<MemSDNode>(Parent);
  // Addresses of the form Addr+const or Addr|const
  if (Node->getPointerInfo().getAddrSpace() != 0 &&
      CurDAG->isBaseWithConstantOffset(Addr)) {
    ConstantSDNode *CN = dyn_cast<ConstantSDNode>(Addr.getOperand(1));
    if (isInt<16>(CN->getSExtValue())) {

      // If the first operand is a FI, get the TargetFI Node
      if (FrameIndexSDNode *FIN =
              dyn_cast<FrameIndexSDNode>(Addr.getOperand(0)))
        Base = CurDAG->getTargetFrameIndex(FIN->getIndex(), MVT::i64);
      else
        Base = Addr.getOperand(0);

      Offset = CurDAG->getTargetConstant(CN->getSExtValue(), DL, MVT::i64);
      return true;
    }
  }

  Base   = Addr;
  Offset = CurDAG->getTargetConstant(0, DL, MVT::i64);
  return true;
}

// ComplexPattern used on BPF FI instruction
bool BPFDAGToDAGISel::SelectFIAddr(SDValue Addr, SDValue &Base, SDValue &Offset) {
  SDLoc DL(Addr);

  if (!CurDAG->isBaseWithConstantOffset(Addr))
    return false;

  // Addresses of the form Addr+const or Addr|const
  ConstantSDNode *CN = dyn_cast<ConstantSDNode>(Addr.getOperand(1));
  if (isInt<16>(CN->getSExtValue())) {

    // If the first operand is a FI, get the TargetFI Node
    if (FrameIndexSDNode *FIN =
            dyn_cast<FrameIndexSDNode>(Addr.getOperand(0)))
      Base = CurDAG->getTargetFrameIndex(FIN->getIndex(), MVT::i64);
    else
      return false;

    Offset = CurDAG->getTargetConstant(CN->getSExtValue(), DL, MVT::i64);
    return true;
  }

  return false;
}

bool BPFDAGToDAGISel::AdjustGuestPointer(MemSDNode *Node, SDValue &Ptr,
                                         SDValue &Offset, SDValue &Chain)
{
  SDLoc DL(Node);

  SDValue R9Reg = CurDAG->getRegister(BPF::R9, MVT::i64);

  // Leave the stack pointers unchanged
  if (Ptr.getOpcode() == ISD::FrameIndex)
    return false;

  // Leave the pointers which are explicitly of the host unchanged
  if (Node->getPointerInfo().getAddrSpace() != 0)
    return false;

  MachineSDNode *NewNode, *HostBaseNode, *GuestBaseNode, *GuestMaskNode;

  // Prepare the host base
  SDValue HostMemBaseOffset = CurDAG->getTargetConstant(0, DL, MVT::i64);
  HostBaseNode = CurDAG->getMachineNode(BPF::LDD, DL, MVT::i64, R9Reg,
      HostMemBaseOffset);
  SDValue GuestMemBaseOffset = CurDAG->getTargetConstant(8, DL, MVT::i64);
  GuestBaseNode = CurDAG->getMachineNode(BPF::LDD, DL, MVT::i64, R9Reg,
      GuestMemBaseOffset);

  SDValue GuestMemMaskOffset = CurDAG->getTargetConstant(16, DL, MVT::i64);
  GuestMaskNode = CurDAG->getMachineNode(BPF::LDD, DL, MVT::i64, R9Reg,
      GuestMemMaskOffset);


    // Add the guest base. It may be inefficient if the Ptr is constant.
    // Future work may address it.
  NewNode = CurDAG->getMachineNode(BPF::SUB_rr, DL, MVT::i64, Ptr,
      SDValue(GuestBaseNode, 0));

  // If the value is know there is no need to mask. Be conservative, assuming
  // the verifier is stupid
  if (Ptr.getOpcode() != ISD::Constant) {
    NewNode = CurDAG->getMachineNode(BPF::AND_rr, DL, MVT::i64,
        SDValue(NewNode, 0), SDValue(GuestMaskNode, 0));
  }

  /* add the host base */
  NewNode = CurDAG->getMachineNode(BPF::ADD_rr, DL, MVT::i64,
      SDValue(NewNode, 0), SDValue(HostBaseNode, 0));

  Ptr = SDValue(NewNode, 0);
  Offset = CurDAG->getTargetConstant(0, DL, MVT::i64);

  return true;
}

void BPFDAGToDAGISel::Select(SDNode *Node) {
  unsigned Opcode = Node->getOpcode();

  // Dump information about the Node being selected
  DEBUG(dbgs() << "Selecting: "; Node->dump(CurDAG); dbgs() << '\n');

  // If we have a custom node, we already have selected!
  if (Node->isMachineOpcode()) {
    DEBUG(dbgs() << "== "; Node->dump(CurDAG); dbgs() << '\n');
    return;
  }

  // tablegen selection should be handled here.
  switch (Opcode) {
  default: break;
  case ISD::SDIV: {
    DebugLoc Empty;
    const DebugLoc &DL = Node->getDebugLoc();
    if (DL != Empty)
      errs() << "Error at line " << DL.getLine() << ": ";
    else
      errs() << "Error: ";
    errs() << "Unsupport signed division for DAG: ";
    Node->dump(CurDAG);
    errs() << "Please convert to unsigned div/mod.\n";
    break;
  }
  case ISD::INTRINSIC_W_CHAIN: {
    unsigned IntNo = cast<ConstantSDNode>(Node->getOperand(1))->getZExtValue();
    switch (IntNo) {
    case Intrinsic::bpf_load_byte:
    case Intrinsic::bpf_load_half:
    case Intrinsic::bpf_load_word: {
      SDLoc DL(Node);
      SDValue Chain = Node->getOperand(0);
      SDValue N1 = Node->getOperand(1);
      SDValue Skb = Node->getOperand(2);
      SDValue N3 = Node->getOperand(3);

      SDValue R6Reg = CurDAG->getRegister(BPF::R6, MVT::i64);
      Chain = CurDAG->getCopyToReg(Chain, DL, R6Reg, Skb, SDValue());
      Node = CurDAG->UpdateNodeOperands(Node, Chain, N1, R6Reg, N3);
      break;
    }
    case Intrinsic::bpf_cache_ctx: {
      SDLoc DL(Node);
      SDValue Chain = Node->getOperand(0);
      SDValue Dst = Node->getOperand(1);
      SDValue Ctx = Node->getOperand(2);

      SDValue R9Reg = CurDAG->getRegister(BPF::R9, MVT::i64);
      Chain = CurDAG->getCopyToReg(Chain, DL, R9Reg, Ctx, SDValue());
      Node = CurDAG->UpdateNodeOperands(Node, Chain, Dst, Ctx);
      break;
    }
    }
    break;
  }

  case ISD::FrameIndex: {
    int FI = cast<FrameIndexSDNode>(Node)->getIndex();
    EVT VT = Node->getValueType(0);
    SDValue TFI = CurDAG->getTargetFrameIndex(FI, VT);
    unsigned Opc = BPF::MOV_rr;
    if (Node->hasOneUse()) {
      CurDAG->SelectNodeTo(Node, Opc, VT, TFI);
      return;
    }
    ReplaceNode(Node, CurDAG->getMachineNode(Opc, SDLoc(Node), VT, TFI));
    return;
  }

  case ISD::LOAD: {
    LoadSDNode *LD = cast<LoadSDNode>(Node);
    SDLoc DL(Node);
    SDValue Chain = LD->getChain();
    SDValue Ptr = LD->getBasePtr();
    SDValue Offset  = LD->getOffset();

    if (AdjustGuestPointer(LD, Ptr, Offset, Chain))
      Node = CurDAG->UpdateNodeOperands(Node, Chain, Ptr, Offset);
    break;
  }

  case ISD::STORE: {
    StoreSDNode *ST = cast<StoreSDNode>(Node);
    SDLoc DL(Node);
    SDValue Chain = ST->getChain();
    SDValue Value = ST->getValue();
    SDValue Ptr = ST->getBasePtr();
    SDValue Offset = ST->getOffset();

    if (AdjustGuestPointer(ST, Ptr, Offset, Chain))
      Node = CurDAG->UpdateNodeOperands(Node, Chain, Value, Ptr, Offset);
    break;
  }

  }

  // Select the default instruction
  SelectCode(Node);
}

FunctionPass *llvm::createBPFISelDag(BPFTargetMachine &TM) {
  return new BPFDAGToDAGISel(TM);
}
